const utils = require('/opt/nodejs/utility-layer/utils');
var https = require('https');

function httpsRequest(options,payload) {
    return new Promise((resolve, reject) => {
        const req = https.request(options, (res) => {
            if (res.statusCode < 200 || res.statusCode >= 300) {
                return reject(new Error('statusCode=' + res.statusCode));
            }
            var body = [];
            res.on('data', function(chunk) {
                body.push(chunk);
            });
            res.on('end', function() {
                try {
                    body = Buffer.concat(body).toString();
                } catch(e) {
                    reject(e);
                }
                resolve(body);
            });
        });
        
        req.on('error', (e) => {
            reject(e.message);
        });
        if(options.method == 'POST'){
            req.write(payload);
        }
        req.end();
    });
}
exports.issueTrophies = async (event) => {
    return new Promise(async (resolve, reject) => {
        try {

            let gitPayload = utils.processEventBody({
                body: event.body,
            });

            let commitId = gitPayload.after;
            var projectUrl = new URL(gitPayload.project.web_url + "/-/raw/" + commitId + "/trophies.tracking.json");
            let payload;
            var options = {
                method: 'GET',
                hostname: projectUrl.hostname,
                path: projectUrl.pathname,
                headers: {},
            };

            try {
                payload = await httpsRequest(options);
                console.log('Completed successfully! Response body:', payload);
            }
            catch (err) {
                console.error('Request failed, error:', err);
                return resolve(utils.formatErrorResponse(
                    401,
                    err
                ));
            }

            let projectId = event.pathParameters.projectId;
            var trophiesIssueUrl = new URL("https://flo1ogawm1.execute-api.af-south-1.amazonaws.com/prod/projects/" + projectId + "/trophies");    

            let apiKey;
            if (event.headers['X-Gitlab-Token']) {
                apiKey = event.headers['X-Gitlab-Token'];
            }else{
                return resolve(utils.formatErrorResponse(
                    401,
                    "No value provided in 'X-Gitlab-Token' header."
                ));
            }

            var options = {
                method: 'POST',
                hostname: trophiesIssueUrl.hostname,
                path: trophiesIssueUrl.pathname,
                headers: {
                    'Content-Type': 'application/json',
                    'apiKey':apiKey
                },
            };

            try {
                const postBody = await httpsRequest(options, payload);
                console.log('Response body:', postBody);
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true,
                        "message": 'Lambda Executed - '+postBody
                    }
                }));
            }
            catch (err) {
                console.error('Request failed, error:', err);
                resolve(utils.formatErrorResponse(
                    500,
                    err
                ));
            }                                  
        } catch (err) {
            console.error(err);
            resolve(utils.formatErrorResponse(
                400,
                err
            ));
        }
    });
};
