import * as cdk from '@aws-cdk/core';
import { RestApi, LambdaIntegration, Cors } from '@aws-cdk/aws-apigateway';
import { Function, Runtime, Code, LayerVersion } from '@aws-cdk/aws-lambda';
import { Rule, Schedule } from '@aws-cdk/aws-events';
import { LambdaFunction } from '@aws-cdk/aws-events-targets';

export class GitLabTrophiesStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps, customOptions?: any) {
    super(scope, id, props);
    const stack = this;
    customOptions = customOptions || {};


    // set default CORS origin to ALL_ORIGINS
    let corsOrigin = customOptions.corsOrigin || "*";
    // make the stack's CORS origin available to lambdas as an environment variable
    let corsEnvironment = {
        CORS_ORIGIN: corsOrigin
    };

    const gitlabTrophiesApi = new RestApi(stack, `gitlab-trophies-api`, {
      defaultCorsPreflightOptions: {
      allowOrigins: [ corsOrigin ],
      allowMethods: Cors.ALL_METHODS,
      }
    });

    // utility layer
    const utilityLayer = new LayerVersion(stack, 'utility-layer', {
      // Code.fromAsset must reference the build folder
      code: Code.fromAsset('./layers/build/utility-layer'),
      compatibleRuntimes: [Runtime.NODEJS_12_X],
      license: 'MIT',
      description: 'a layer for providing utility functions',
    });

    // set up api resources
    const api: any = {
      'root': gitlabTrophiesApi.root
    };

    // /projects
    api.projects = api.root.addResource('projects');
    // /projects/{projectId}
    api.project = api.projects.addResource('{projectId}');

    // /trophies
    api.trophies = api.root.addResource('trophies');
  
    // /projects/{projectId}/trophies
    api.projectTrophies = api.project.addResource('trophies');

    let trophyFunctions = [
    {
        name: 'gitlab-issue-trophies',
        handler: 'issue.issueTrophies',
        code: './handlers/trophies',
        method: 'POST',
        resource: api.projectTrophies,
        environment: {
            ...corsEnvironment
        },
        layers: [utilityLayer],
    }];

    for (let tfi in trophyFunctions) {
      createIntegratedFunction(stack, trophyFunctions[tfi]);
    }
  }
}

function createIntegratedFunction(stack: GitLabTrophiesStack, definition: any) {
  console.log(`creating lambda ${definition.name}...`);
  let lambda = new Function(stack, `${definition.name}-function`, {
      runtime: Runtime.NODEJS_12_X,
      handler: definition.handler,
      code: Code.fromAsset(definition.code),
      environment: definition.environment,
      layers: definition.layers,
      timeout: definition.timeout || cdk.Duration.seconds(5)
  });

  if (!definition.methods && definition.method) {
      definition.methods = [ definition.method ];
  }

  if (!definition.resources && definition.resource) {
      definition.resources = [ definition.resource ];
  }

  let lambdaIntegration = new LambdaIntegration(lambda);
  for (let ri in definition.resources) {
      let resource = definition.resources[ri];
      for (let mi in definition.methods) {
          let method = definition.methods[mi];
          console.log(`adding method ${method} to resource ${resource}...`);
          resource.addMethod(method, lambdaIntegration);
      }
  }

  let queueAccess = definition.queueAccess || [];
  for (let qai in queueAccess) {
      definition.queueAccess[qai].grantSendMessages(lambda);
  }

  let eventSources = definition.eventSources || [];
  for (let esi in eventSources) {
      lambda.addEventSource(eventSources[esi]);
  }

  if (definition.cron) {
      let rule = new Rule(stack, `${definition.name}-rule`, {
          schedule: definition.cron
      });

      rule.addTarget(new LambdaFunction(lambda));
  }
}
