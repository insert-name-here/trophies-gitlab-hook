# Trophies manager - GitLab Hook

A serverless token issuing solution for [Trophies](https://gitlab.com/insert-name-here/trophies) for Gitlab push event webhook.

## Useful commands

- `npm run build`   build layers and compile typescript to js
- `npm run synth`   perform build steps then synthesize the CloudFormation template(s)
- `cdk deploy`      deploy this stack to your default AWS account/region
- `cdk diff`        compare deployed stack with current state

## API documentation

This endpoint is not really meant to work outside of the Gitlab Webhook integration

## Hook Endpoint

#### POST {{host}}/projects/{{projectId}}/trophies

The X-Gitlab-Token HTTP (setup via the GitLab Webhook Secret Token) is REQUIRED which will represent the "apiKey" within the handler.

Trophies can be issued automatically by a repository on PR merge using this endpoint.

Request format:

`The Gitlab merge event payload see` [Gitlab Push event](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#push-events)

Response format:

`A 200 will be returned from the lambda to the Webhook, this can be viewed in the logs of the webhook on Gitlab.`
